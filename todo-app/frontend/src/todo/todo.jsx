import React, { Component } from 'react';
import Axios from 'axios';

import PageHeader from '../template/pageHeader';
import TodoForm from './todoForm';
import TodoList from '../todo/todoList';


const URL = 'http://localhost:3003/api/todos';

export default class Todo extends Component {

    constructor(props){
        super(props);

        //estado inicial do objeto
        this.state = { description: '', list: [] };

        // importante fazer o bind para que o "this" referente seja o "this" da classe "Todo"
        // se isso não for feito, o React entende que o "this" é "undefined" o que causará um erro na 
        // execução da função
        this.handleChange = this.handleChange.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleMArkAsDone = this.handleMArkAsDone.bind(this);
        this.handleMarkAsPending = this.handleMarkAsPending.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleClear = this.handleClear.bind(this);

        this.refresh();
    }

    refresh(description = ''){
        const search = description ? `&description__regex=/${description}/` : '';
        Axios.get(`${URL}?sort=-createAt${search}`)
            .then(resp => this.setState({ ...this.state, description, list: resp.data }));
    }

    handleSearch(){
        this.refresh(this.state.description);
    }

    handleChange(e){
        // faz um clone do objeto atual atualizando o valor da descrição
        // quando iniciamos o componente através de um "estado", se ele não for alterado
        // mesmo que o evento seja disparado, o estado do objeto não é evoluido, sendo assim, os dados do componente
        // não são atualizados
        this.setState({ ...this.state, description: e.target.value });
    }

    handleAdd(){
        const description = this.state.description;
        Axios.post(URL, { description })
            .then(resp => this.refresh())
    }

    handleRemove(todo){
        Axios.delete(`${URL}/${todo._id}`)
            .then(resp => this.refresh(this.state.description));
    }

    handleMArkAsDone(todo){
        Axios.put(`${URL}/${todo._id}`, { ...todo, done: true })
            .then(resp => this.refresh(this.state.description));
    }

    handleMarkAsPending(todo){
        Axios.put(`${URL}/${todo._id}`, { ...todo, done: false })
            .then(resp => this.refresh(this.state.description));
    }

    handleClear(){
        this.refresh();
    }

    render(){
        return (
            <div>
                <PageHeader name="Tarefas" small="Cadastro" />
                <TodoForm 
                    description={this.state.description}
                    handleChange={this.handleChange}
                    handleAdd={this.handleAdd} 
                    handleSearch={this.handleSearch} 
                    handleClear={this.handleClear} />
                <TodoList 
                    list={this.state.list} 
                    handleRemove={this.handleRemove}
                    handleMArkAsDone={this.handleMArkAsDone}
                    handleMarkAsPending={this.handleMarkAsPending} /> 
            </div>
        )
    }
}