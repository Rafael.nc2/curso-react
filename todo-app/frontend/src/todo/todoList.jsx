import React from 'react';

import IconButton from '../template/iconButton';

export default props => {

    const renderRows = () =>{
        const list = props.list || [];
        return list.map(todo =>(
            <tr key={todo._id}>
                <td className={ todo.done ? 'markAsDOne' : '' }>{todo.description}</td>
                <td>
                    {/* 
                        Como estou passando o "todo" atual como parâmetro para a função, é necessário
                        chamar a função através de uma arrow function
                    */}
                    <IconButton style="success" icon="check" hide={todo.done}
                        onClick={() => props.handleMArkAsDone(todo)}></IconButton>

                    <IconButton style="warning" icon="undo" hide={!todo.done}
                        onClick={() => props.handleMarkAsPending(todo)}></IconButton>

                    <IconButton style="danger" icon="trash-o" hide={!todo.done}
                        onClick={() => props.handleRemove(todo)}></IconButton>
                </td>
            </tr>
        ))
    }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th className="tableActions">Ações</th>
                </tr>
            </thead>
            <tbody>
                {renderRows()}
            </tbody>
        </table>
    )
}