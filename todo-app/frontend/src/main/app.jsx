import 'modules/bootstrap/dist/css/bootstrap.min.css';
import 'modules/font-awesome/css/font-awesome.min.css';
import '../template/custom.css';

import React from 'react';

import Menu from '../template/menu';
import Routes from './routes';

// o uso dos parenteses inidca o retorno de uma expressão;
// Lembrando que em arrow function o "return" é implicíto, por isso não é necessário colocar s chaves {};
// Caso queira utilizar chave, devemos colocar o return;

export default props => (
    <div className="container">
        <Menu />
        <Routes />
    </div>
)