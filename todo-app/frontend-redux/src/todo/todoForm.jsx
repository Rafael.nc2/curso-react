import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Grid from '../template/grid';
import IconButton from '../template/iconButton';

import { add, changeDescription, search, clear } from './todoActions';

class TodoForm extends Component {
    constructor(props){
        super(props);

        this.keyHandler = this.keyHandler.bind(this);
    }

    // o método "componentWillMount" sempre é executado quando o componente for montado
    // porém, este método só é permitido dentro de componentes de classe, não funciona em componentes
    // funcionais
    componentWillMount(){
        this.props.search();
    }

    keyHandler(e){
        // extrai, através do destructuring (ES6), os métodos "add", "search" e também o
        // atributo "description" para serem utilizados diretamente da variável
        const { add, clear, search, description } = this.props;
        if(e.key === 'Enter'){
            e.shiftKey ? search() : add(description);
        }else if(e.key === 'Escape'){
            clear();
        }
    }

    render(){
        const { add, search, description } = this.props;

        return (
            <div role="form" className="todoForm">
                <Grid cols = "12 9 10">
                    <input id="description" className="form-control"
                        placeholder="Adicione uma tarefa" 
                        onKeyUp={this.keyHandler}
                        onChange={this.props.changeDescription}
                        value={this.props.description}></input>
                </Grid>
        
                <Grid cols="12 3 2">
                    <IconButton style='primary' icon='plus' onClick={ () => add(description) }></IconButton>
                    
                    <IconButton style='info' icon='search' onClick={ search }></IconButton>
                    
                    <IconButton style='default' icon='close' onClick={this.props.clear}></IconButton>
                </Grid>
            </div>
        );
    }
}


// Liga o estado da aplicação com o props do componente
const mapStateToProps = state => ({ description: state.todo.description });
// Liga as ações criadas no actionCretors com o props.do componente
const mapDispatchToProps = dispatch => 
    bindActionCreators({ add, changeDescription, search, clear }, dispatch);
// Export o componente com tudo ja ligado ao seu props
export default connect(mapStateToProps, mapDispatchToProps)(TodoForm);