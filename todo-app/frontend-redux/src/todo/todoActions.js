import axios from 'axios';

const URL = 'http://localhost:3003/api/todos';

export const changeDescription = event => ({
    type: 'DESCRIPTION_CHANGED',
    payload: event.target.value
})

export const search = (description) => {
    // para não limpar a consulta quando executar alguma ação no form, 
    // devemos acessar o valor de "description" que está no state, para isso utilizamos o método "getState"
    return (dispatch, getState) => {
        const description = getState().todo.description;
        const search = description ? `&description__regex=/${description}/` : '';
        const request = axios.get(`${URL}?sort=-createAt${search}`)
                .then(resp => dispatch({ type: 'TODO_SEARCHED', payload: resp.data}))
    }
}

export const add = description => {
    return dispatch => {
        // só é possível resolver a promise desta forma devido ao middleware "thunk"
        // assim garantimos a sequencia correta de execução das promises
        axios.post(URL, { description })
            // .then(resp => dispatch({ type: 'TODO_ADDED', payload: resp.data }))
            .then(resp => dispatch(clear()))
            .then(resp => dispatch(search()))
    }
}

export const markAsDone = todo => {
    return dispatch => {
        axios.put(`${URL}/${todo._id}`, { ...todo, done: true })
            // .then(resp => dispatch({ type: 'TODO_MARKED_AS_DONE', payload: resp.data }))
            .then(resp => dispatch(search()))
    }
}

export const markAsPending = todo => {
    return dispatch => {
        axios.put(`${URL}/${todo._id}`, { ...todo, done: false })
            // .then(resp => dispatch({ type: 'TODO_MARKED_AS_PENDING', payload: resp.data }))
            .then(resp => dispatch(search()))
    }
}

export const remove = todo => {
    return dispatch => {
        axios.delete(`${URL}/${todo._id}`)
            // .then(resp => dispatch({ type: 'TODO_REMOVED', payload: resp.data }))
            .then(resp => dispatch(search()));
    }
}

export const clear = () => {
    // lembrando que só é possível executar mais de uma action, passando através de um array,
    // devido ao middleware "multi"
    return [{ type: 'TODO_CLEAR' }, search()]
}