import React from 'react';
import ReactDOM from 'react-dom';

// applyMiddleware = para permitir a utilização de middlewares
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';

// Middlewares
// importa o middleware que resolve promises antes de disparar os reducers
import promise from 'redux-promise';
// importa o middleware que executa um array de actions
import multi from 'redux-multi';
// importa o middleware que faz com que uma ação só seja disparada após o término da anterior
// este middleware faz com que retorne não mais uma action, mas sim um método que recebe como 
// parâmetro um "dispatch"
import thunk from 'redux-thunk';

import App from  './main/app';
import reducers from './main/reducers';

// Adicionar o plugin Redux DenTools no chorme
// para utilizar o plugin, devemos instanciar a variável abaixo e passar como parâmetro no create store
// junto com os reducers
const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

// cria a store permitindo utilizar o middleware
const store = applyMiddleware(thunk, multi, promise)(createStore)(reducers, devTools);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    ,document.getElementById('app')
);