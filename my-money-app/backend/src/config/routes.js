const express = require('express');
const auth = require('./auth')

module.exports = function (server) {

    /*
    * Rotas protegidas por Token JWT
    */
    const protectedApi = express.Router()
    server.use('/api', protectedApi)

    protectedApi.use(auth)

    // Definir URL base para todasa as rotas
    // const router = express.Router();
    // Todas as URLs que começarem com "/api" serão direcionadas para o "router"
    // server.use('/api', router)

    // Rotas relacionadas ao Ciclo de Pagamento
    const BillingCycle = require('../api/billingCycle/billingCycleService');
    // BillingCycle.register(router, '/billingCycles');
    BillingCycle.register(protectedApi, '/billingCycles')

    // Como ja foi informado quais tipos de requisição serão utilizados, apenas registramos 
    // com o "register" do schema, qual será o roteamento utilizado.
    // Dessa forma, dispensamos colocar cada um dos verbos 

    /*
    * Rotas abertas
    */
    const openApi = express.Router()
    server.use('/oapi', openApi)
    
    const AuthService = require('../api/user/AuthService')
    
    openApi.post('/login', AuthService.login)
    openApi.post('/signup', AuthService.signup)
    openApi.post('/validateToken', AuthService.validateToken)

}