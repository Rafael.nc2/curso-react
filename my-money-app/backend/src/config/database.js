const mongoose =  require('mongoose');

// a API de promise do mongoose está "deprecated" por esse motivo, inofrmamos ao mongoose
// para utilizar a promise global do node. Isso evita vários warnings e quando realmente a promise
// do mongoose for descontinuada, não teremos nenhum problema na aplicação.
mongoose.Promise = global.Promise;
module.exports = mongoose.connect('mongodb://curso-react:curso-react-2018@ds045087.mlab.com:45087/curso-react?readPreference=primary', { useMongoClient: true });

// Personalizando as mensagens de erro
mongoose.Error.messages.general.required = "O atributo '{PATH}' é obrigatório!";
mongoose.Error.messages.Number.min = "O '{VALUE}' informado é menor que o limite mínimo de '{MIN}'!";
mongoose.Error.messages.Number.max = "O '{VALUE}' informado é maior que o limite máximo de '{MAX}'!";
mongoose.Error.messages.String.enum = "'{VALUE}' não é válido para o atributo '{PATH}'!";