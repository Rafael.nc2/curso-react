const BillingCycle = require('./billingCycle');
const errorHandler = require('../common/errorHandler');

// mapeando os tipos de requisição que queremos utilizar
BillingCycle.methods(['get','post','put','delete']);

// node-restful, por padrão, aplica as restrições inseridas no schema. 
// Porém, se tratando do método "PUT", as restrições são ignoradas.
// Para alterar este comportamento, devemos alterar as configurações de atualização da seguinte forma:
BillingCycle.updateOptions({ new: true, runValidators: true })
// new: true = faz com que o node-restful traga a nova versão do objeto, ou seja, o objeto depois de atualizado
// runValidators: true = faz com que o node-restful aplique as validações também na atualização do objeto

// BillingCycle.after = intercepta a requisição após a chamada. No caso abaixo, intercepta a chamada após
// uma requisição do tipo "post" e "put" para manipular os erros, caso tenham
BillingCycle.after('post', errorHandler).after('put', errorHandler);

// É possível também, interceptar as requisições antes de serem acionadas
// para um caso de validação de token, por exemplo
// BillingCycle.before

// Para mais de uma rota
// Para definir um único verbo para a rota, temos duas opções:
// BillingCycle.route('count.get', (req, res, next) => {
// BillingCycle.route('count', ['get'], (req, res, next) => {
// Sem especificar o verbo, a rota fica disponível em todos os verbos HTTP

BillingCycle.route('count', (req, res, next) => {
    BillingCycle.count( (error, value) => {
        if( error ){
            res.status(500).json({ errors: [error] });
        }else{
            res.json({value})
        }
    });
});

BillingCycle.route('summary', (req, res, next) => {
    BillingCycle.aggregate({
        $project: {credit: {$sum: "$credits.value"}, debt: {$sum: "$debts.value"}}
    },{
        $group: {_id: null, credit: {$sum: "$credit"}, debt: {$sum: "$debt"}}
    },{
        $project: {_id: 0, credit: 1, debt: 1}
    }, (error, result) => {
        if(error){
            res.status(500).json({errors: [error]})
        }else{
            res.json(result[0] || { credit: 0, debt: 0 })
        }
    });
});

module.exports = BillingCycle;