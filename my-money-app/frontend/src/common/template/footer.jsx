import React from 'react';

export default props => (
    <footer className="main-footer">
        <strong>
            Copyright &copy; 2018
            <a href="http://www.rafaelnaville.com.br" target="_blank"> RafaelNaville</a>.
        </strong>
    </footer>
)