import types from '../actions/actionTypes';

const initialState = {
    min: 7,
    max: 31
};

export default function (state = initialState, action) {
    switch (action.type) {
        case types.NUM_MIN_ALTERADO:
            return { ...state, min: action.payload }

        case types.NUM_MAX_ALTERADO:
            return { ...state, max: action.payload }

        default:
            return state;
    }
}