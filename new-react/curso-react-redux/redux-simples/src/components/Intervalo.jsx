import './Intervalo.css';
import React from 'react';
import { connect } from 'react-redux';
import { alterarValorMinimo, alterarValorMaximo } from '../store/actions/numeros';

import Card from './Card';

const Intervalo = props => {
    const { min, max, alterarMinimo, alterarMaximo } = props;

    return (
        <Card title="Intervalo de números" red>
            <div className="Intervalo">
                <span>
                    <strong>Mínimo:</strong>
                    <input type="number" value={min} 
                        onChange={e => alterarMinimo(+e.target.value)} 
                    />
                </span>
                <span>
                    <strong>Máximo:</strong>
                    <input type="number" value={max} 
                        onChange={e => alterarMaximo(+e.target.value)} 
                    />
                </span>

            </div>
        </Card>
    )
};

const mapStateToProps = state => ({
    min: state.numeros.min,
    max: state.numeros.max
});

const mapDispatchToProps = dispatch => ({
    alterarMinimo(novoNumero) {
        // action creator
        const action = alterarValorMinimo(novoNumero);
        dispatch(action);
    },
    alterarMaximo(novoNumero) {
        // action creator
        const action = alterarValorMaximo(novoNumero);
        dispatch(action);
    },
});

export default connect(
    mapStateToProps, 
    mapDispatchToProps
)(Intervalo);