export function numberReducer(state, action) {
    switch(action.type) {
        case 'numberAdd2': 
            return {...state, number: state.number + 2};
        default:
            return state;
    }
}