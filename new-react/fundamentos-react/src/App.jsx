import './App.css';
import React from 'react';

import Card from './components/layout/Card';
import Primeiro from './components/basicos/Primeiro';
import ComParametro from './components/basicos/ComParametro';
import Aleatorio from './components/basicos/Aleatorio';
import Familia from './components/basicos/Familia';
import FamiliaMembro from './components/basicos/FamiliaMembro';
import ListaAlunos from './components/repeticao/ListaAlunos';
import ParOuImpar from './components/condicional/ParOuImpar';
import UsuarioInfo from './components/condicional/UsuarioInfo';
import UsuarioInfoElse from './components/condicional/UsuarioInfoElse';
import IndiretaPai from './components/comunicacao/IndiretaPai';

export default _ => (
    <div className="App">
        <h1>New Fundamentos React</h1>

        <div className="Cards">

            <Card titulo="Comunicaçã Indireta" color="#FA6900">
                <IndiretaPai />
            </Card>

            <Card titulo="Reder condicional" color="#588C73">
                <UsuarioInfoElse usuario={{ name: 'Rafael' }} />
                <UsuarioInfo usuario={{ nome: 'Rafael' }} />
                <ParOuImpar numero={20} />
            </Card>

            <Card titulo="Repetição" color="#FF4C65">
                <ListaAlunos />
            </Card>

            <Card titulo="Familia" color="#E94C6F">
                <Familia sobrenome="Naville">
                    <FamiliaMembro nome="Natália" />
                    <FamiliaMembro nome="Helena" />
                    <FamiliaMembro nome="Rafael" />
                </Familia>
            </Card>
            
            <Card titulo="Aleatorio" color="#FA6900">
                <Aleatorio
                    min={1}
                    max={10}
                />
            </Card>

            <Card titulo="Primeiro" color="#E94C6F">
                <Primeiro />
            </Card>
            <Card titulo="ComParametro" color="#588C73">
                <ComParametro
                    titulo="Situação do Aluno"
                    aluno="Natália"
                    nota={9.9}
                />
                <ComParametro
                    titulo="Situação do Aluno"
                    aluno="Helena"
                    nota={9.9}
                />
            </Card>

        </div>
    </div>
);
