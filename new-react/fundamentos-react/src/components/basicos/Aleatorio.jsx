export default ({ min, max }) => {
    const numeroAleatorio = parseInt(Math.random() * (max - min)) + min;

    return (
        `O número entre ${min} e ${max} é ${numeroAleatorio}`
    );
}