import React, { cloneElement } from 'react';

export default props => {
    return (
        <div>
            { 
                props.children.map((child, i) => 
                    cloneElement(child, { ...props, key: i })
                )
            }
        </div>
    );
}
