import React, { useState } from 'react';
import IndiretaFilho from './IndiretaFilho';

export default props => {
    const [nome, setNome] = useState('');
    const [idade, setIdade] = useState(0);
    const [nerd, setNerd] = useState(false);

    function fornecerInformacoes(nomeParam, idadeParam, nerdParam) {
        setNome(nomeParam);
        setIdade(idadeParam);
        setNerd(nerdParam)
    }

    return (
        <div>
            <div>
                Pai
                <p>{nome}</p>
                <p>{idade}</p>
                <p>{nerd ? 'Verdadeiro' : 'Falso'}</p>
            </div>
            <IndiretaFilho quandoClicar={fornecerInformacoes} />
        </div>
    );
}