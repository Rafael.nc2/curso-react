/**
 * <IF teste={aluno.nota > 7}>
 *  <span>;;</span>
 * </IF>
 * 
 */
export default props => {
    if(props.test) {
        return props.children;
    }else {
        return false;
    }
}
