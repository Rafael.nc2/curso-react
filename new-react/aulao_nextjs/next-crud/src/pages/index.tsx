import Botao from '../components/Botao';
import Formulario from '../components/Formulario';
import Layout from '../components/Layout';
import Tabela from '../components/Tabela';
import Cliente from '../core/Cliente';

export default function Home() {

	const clientes = [
		new Cliente('Natália', 4, '1'),
		new Cliente('Helena', 38, '2'),
		new Cliente('Rafael', 36, '2')
	];

	function clienteSelecionado(cliente: Cliente) {
		console.log('Selecionar: ', cliente.nome);
	}
	function clienteExcluido(cliente: Cliente) {
		console.log('Excluir: ', cliente.nome);
	}

	return (
		<div className={`
			flex justify-center items-center h-screen
			bg-gradient-to-r from-blue-500 to-purple-500
			text-white
		`}>
			<Layout titulo="Cadastro Simples">
				<div className="flex justify-end">
					<Botao cor="green" className="mb-4">
						Novo Cliente
					</Botao>

				</div>
				<Formulario cliente={clientes[0]} />
				{/* <Tabela 
					clientes={clientes}
					clienteSelecionado={clienteSelecionado}
					clienteExcluido={clienteExcluido}
				></Tabela> */}
			</Layout>
		</div>
	)
}
