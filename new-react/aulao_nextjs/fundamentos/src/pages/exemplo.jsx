import Layout from '../components/Layout';
import Cabecalho from "../components/Cabecalho";

export default function Exemplo() {
    return (
        <Layout titulo="Usando componentes">
            <Cabecalho titulo="Next.Js & react" />
            <Cabecalho titulo="Aprenda next na prática" />
        </Layout>
    );
}