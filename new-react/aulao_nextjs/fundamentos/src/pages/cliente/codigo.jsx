//import { useEffect } from "react";
import Layout from "../../components/Layout";
import { useRouter } from 'next/router';
//import router from 'next/router';


export default function ClientePorCodigo() {
    const router = useRouter();

    // useEffect(() => {
    //     console.log(router.query);
    // }, []);

    return (
        <Layout titulo="Navegação dinâmica">
            <span>Codigo = {router.query.codigo}</span>
        </Layout>
    );
}