import React from 'react';

// componentes podem ser criados baseados em função ou baseados em classes
// parenteses é utilizado para retornar uma expressão
export default () => (
    <h1>Primeiro componente!</h1>
);