import React, { Component } from 'react';

// importante:
// o componente react baseado em classe, deve conter pelo menos uma função, que é a função "render";
// em classes, não é possível mudar o nome da variável "props" pois, essa variável é herdada
// a partir da classe "Component", sendo assim, esse é o nome padrão para todas as classes
// componentes classe são mais indicados para componentes complexos, com muitos métodos e quando
// você quer que o componente tenha um estado
export default class ClassComponent extends Component {

    render(){
        return (
            <h1>{ this.props.value }</h1>
        )
    }

}