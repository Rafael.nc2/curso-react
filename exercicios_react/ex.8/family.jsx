import React from 'react';

// neste formato, o cloneElement passa todas propriedades do pai para o filho
// isso não é considerado uma boa prática, o ideal é passar um clone do "props" através do 
// operador "spread"
export default props => (
    <div>
        <h1>Família</h1>
        {/* { React.cloneElement(props.children, props) } */}
        { React.cloneElement(props.children, { ...props }) }
    </div>
)