import React from 'react';
import { childrenWithProps } from '../utils/reactUtils';

// neste formato, o cloneElement passa todas propriedades do pai para o filho
// isso não é considerado uma boa prática, o ideal é passar um clone do "props" através do 
// operador "spread"
// cloneElement funciona apenas quando temos um único elemento filho, pois ele espera uma string ou
// um componente.
//Para resolver, devemos utilizar o "React.children.map", muito parecido com o "map" do javascript

// toda vez que for colocar alguma expressão, chamada de método ou qualquer comando do javascript,
// devemos colocar entre chaves para identificar 
export default props => (
    <div>
        <h1>Família</h1>
        {/* { React.cloneElement(props.children, props) } */}
        {/* { React.cloneElement(props.children, { ...props }) } */}
        {/* { React.Children.map(props.children, 
            child => React.cloneElement(child, { ...props })) } */}
        { childrenWithProps(props.children, props) }
    </div>
)