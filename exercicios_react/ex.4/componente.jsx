import React from 'react';

// componentes podem ser criados baseados em função ou baseados em classes
// parenteses é utilizado para retornar uma expressão
// props é uma convensão, podemos utilizar qualquer variável

export default props => (
    <h1>{props.value}</h1>
);