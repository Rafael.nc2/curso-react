import React from 'react';
import ReactDOM from 'react-dom';

import Componente from './componente'

// ReactDOM só é necessário quando realmente for jogar os componentes para dentro da página HTML
ReactDOM.render( <Componente value='Show!' />, document.getElementById('app'));