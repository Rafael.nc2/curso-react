import React, { Component } from 'react';

// importante:
// o componente react baseado em classe, deve conter pelo menos uma função, que é a função "render";
// em classes, não é possível mudar o nome da variável "props" pois, essa variável é herdada
// a partir da classe "Component", sendo assim, esse é o nome padrão para todas as classes
// componentes classe são mais indicados para componentes complexos, com muitos métodos e quando
// você quer que o componente tenha um estado

// todos os valores que estão dentro de "props" são "somente leitura", sendo assim, não é possível modifica-los
// diretamente

export default class ClassComponent extends Component {

    constructor(props){
        // super é para referenciar ao "props" que está na super classe (Component)
        super(props);
        // a propriedade state existe em todos os compoenentes e também vem da super classe
        // neste momento estamos inicializando o estado do componente
        this.state = { value: props.initialValue };
    }

    sum(delta){
        // só é possível alterar o valor do "state" através da função "setState" passando por parâmetro
        // um novo objeto
        // um dos principios do react (programação funcional) é que o dado não é alterado e sim evoluído
        // por isso passamos um novo objeto como parâmetro e não alteramos o que ja existe 
        this.setState({ value: this.state.value + delta });
        
        // quando trabalhamos com mais de um atributo, passamos, através do operador spread, o state atual
        // e em seguida alteramos a valor do atributo desejado
        // this.setState({ ...this.state, value: this.state.value + delta });
    }

    render(){
        return (
            // <h1>{ this.props.value }</h1>
            // () => this.sum(-1) = retorna a função em sí e não tenta executá-la para servir como "onClick" do botão
            <div>
                <h1>{ this.props.label }</h1>
                <h2>{ this.state.value }</h2>
                <button onClick={ () => this.sum(-1) }>Dec</button>
                <button onClick={ () => this.sum(1) }>Dec</button>
            </div>
        )
    }

}