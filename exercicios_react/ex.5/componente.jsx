import React from 'react';

// via de regra, os componentes são separados por arquivos, porém é possível criar mais de um
// componente em um único arquivo

export default props => (
    <h1>Primeiro componente!</h1>
);

export const Segundo = props => <h1>Segundo componente!</h1>;

// quando tiver mais de um componente, não é necessário exportar com o "default"

// export { Primeiro, Segundo };