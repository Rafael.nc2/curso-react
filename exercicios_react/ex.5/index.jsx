import React from 'react';
import ReactDOM from 'react-dom';

import Primeiro, { Segundo } from './componente'

// ReactDOM só é necessário quando realmente for jogar os componentes para dentro da página HTML
ReactDOM.render( 
    <div>
        <Primeiro />
        <Segundo />
    </div>
    , document.getElementById('app')
);