import React, { Component } from 'react';
import { connect } from 'react-redux';
// connect = conecta o React (componentes) com o Redux (gerenciamento de estados)

import { bindActionCreators } from 'redux';
// bindActionCreators = liga as ações criadas aos "dispatchers" (quem está disparando as ações)

import { changeValue } from './fieldActions';

class Field extends Component{
    // Toda vez que o "state" é atualizado, a função "render" é chamada para atualizar a tela
    render(){
        return (
            <div>
                <label>{ this.props.value }</label><br />
                <input onChange={ this.props.changeValue } value={ this.props.value } />
            </div>
        )
    }
}

// com a utilização do redux, não faz mais sentido utilizar o "state", que é o estado global da aplicação, 
// diretamente no componente. Por isso, é necessário mapear os atributos do estado para dentro da variavel "props"
// isso deve ser feito com a função abaixo
function mapStateToProps(state){
    return {
        value: state.field.value
    }
}

// assim como os atributos, devemos também mapear as ações/funções. Para isso utilizamos a função abaixo
// passando como parâmetro, que está disparando a ação
function mapDispatchToProps(dispatch){
    // ligação entre a ação e o disparador
    return bindActionCreators({ changeValue }, dispatch);
}

// padrão "decorator" de desenvolvimento
// exporta o componente field com as atribuições da função "mapStateToProps"
export default connect(mapStateToProps, mapDispatchToProps)(Field);