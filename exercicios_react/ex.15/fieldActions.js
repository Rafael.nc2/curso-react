export function changeValue(e){
    console.log('changeValue');
    // uma função "reducer" sempre deve retornar um objeto com os atributos:
    // type = que informa qual foi a ação - O NOME DO ATRIBUTO SEMPRE DEVE SER TYPE
    // payload = informa qual é o novo valor
    return {
        type: 'VALUE_CHANGED',
        payload: e.target.value
    }
}