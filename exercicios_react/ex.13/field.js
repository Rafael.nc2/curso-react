import React, { Component } from 'react';

class Field extends Component{

    constructor(props){
        super(props);
        this.state = { value: props.initialValue };

        // não sei o por quê mas, quando termino a linha abaixo com ponto e virgula (;) a função
        // handleChange não funciona 
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event){
        this.setState({ value: event.target.value });
    }

    // Toda vez que o "state" é atualizado, a função "render" é chamada para atualizar a tela
    render(){
        return (
            <div>
                <label>{ this.state.value }</label><br />
                <input onChange={ this.handleChange } value={ this.state.value } />
            </div>
        )
    }
}

export default Field;