import React from 'react';
import ReactDOM from 'react-dom';

import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';

// Provider = será um componente
// combineReducers, createStore = são métodos que serão criados 
// createStore = são métodos que serão utilizaods para criar a "store" (estado da aplicação) 
// combineReducers = combina todos os "reducers" - método que acompanha um atributo do estado - 
//                   para a criação do store
// IMPORTANTE: "REDUCERS SÃO FUNÇÕES PURAS" que recebem  por parâmetro, o estado original, 
//             a ação que ocorreu e, baseado nessas informações, evolui o estado - 
//             gera uma versão atualizada do estado atual

import Field from './field';


const reducers = combineReducers({
    // como field deve retornar um objeto, o retorno deve ser colocado entre parenteses para não confundir 
    // o retorno com o corpo da função
    field: () => ({ value: "Opa" })
})

ReactDOM.render( 
    <Provider store={ createStore(reducers) }>
        <Field initialValue="Teste" />
    </Provider>
    ,document.getElementById('app')
);