import React, { Component } from 'react';
import { connect } from 'react-redux';

// connect = conecta o React (componentes) com o Redux (gerenciamento de estados)

class Field extends Component{
    // Toda vez que o "state" é atualizado, a função "render" é chamada para atualizar a tela
    render(){
        return (
            <div>
                <label>{ this.props.valorRecebido }</label><br />
                <input onChange={ this.handleChange } value={ this.props.valorRecebido } />
            </div>
        )
    }
}

// com a utilização do redux, não faz mais sentido utilizar o "state", que é o estado global da aplicação, 
// diretamente no componente. Por isso, é necessário mapear os atributos do estado para dentro da variavel "props"
// isso deve ser feito com a função abaixo
function mapStateToProps(state){
    return {
        valorRecebido: state.field.valorRedux
    }
}

// padrão "decorator" de desenvolvimento
// exporta o componente field com as atribuições da função "mapStateToProps"
export default connect(mapStateToProps)(Field);