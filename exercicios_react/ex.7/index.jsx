import React from 'react';
import ReactDOM from 'react-dom';

import Family from './family';
import Member from './member';

ReactDOM.render( 
    <Family>
        <Member name="Natália" lastname='Naville' />
    </Family>
    , document.getElementById('app')
);