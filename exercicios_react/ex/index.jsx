import React from 'react';
import ReactDOM from 'react-dom';

import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';

import counterReducer from './counterReducer';
import Counter from './counter';

// Provider = será um componente
// combineReducers, createStore = são métodos que serão criados 
// createStore = são métodos que serão utilizaods para criar a "store" (estado da aplicação) 
// combineReducers = combina todos os "reducers" - método que acompanha um atributo do estado - 
//                   para a criação do store
// IMPORTANTE: "REDUCERS SÃO FUNÇÕES PURAS" que recebem  por parâmetro, o estado original, 
//             a ação que ocorreu e, baseado nessas informações, evolui o estado - 
//             gera uma versão atualizada do estado atual




const reducers = combineReducers({
    counter: counterReducer
})

ReactDOM.render( 
    <Provider store={ createStore(reducers) }>
        <Counter />
    </Provider>
    ,document.getElementById('app')
);