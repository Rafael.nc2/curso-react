// REDUCER É A FUNÇÃO PURA QUE RECEBE O ESTADO ATUAL E A FUNÇÃO

const INITIAL_STATE = { step: 1, number: 0 };

export default function(state = INITIAL_STATE, action ){
    // sempre devolve uma evolução do estado atual
    // +action.payload - o sinal de mais "+" serve para indicar que o valor é um número e não uma string
    switch(action.type){
        case 'INC':
            return { ...state, number: state.number + state.step }
        case 'DEC':
            return { ...state, number: state.number - state.step }
        case 'STEP_CHANGED':
            return { ...state, step: +action.payload }
        default:
            return state
    }
}