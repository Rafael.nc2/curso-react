const express = require('express');
const server = express();

// utilizando o "use" ao invés do "verbo" específico, todas as requisições para a url 
// serão atendidas por ele 

// define qual será o inicio da url que será atendida

// caso a url não seja especificada, todas as urls serão atendidas pelo "use"

server.use('/api', function(req, res, next) {
    console.log('Inicio....');
    next();
    console.log('Fim.');
})

server.use('/api', function(req, res) {
    console.log('Resposta.....');
    res.send("<h1>API!</h1>")
})

server.listen(3000, () => console.log('Executando....'));